export const environment = {
  production: true,
  MICROSERVICIO_PRODUCTO : 'http://138.68.246.219:5001/api/v1',
  MICROSERVICIO_REQUISICION : 'http://138.68.246.219:5002/api/v1',
  MICROSERVICIO_COMPRAS : 'http://138.68.246.219:5003/api/v1',
  MICROSERVICIO_USUARIO : 'http://138.68.246.219:5004/api/v1'
};
