import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../../../services/storage.service';

@Component({
  selector: 'main-sidebar-header',
  templateUrl: './main-sidebar-header.component.html',
  styleUrls: ['./main-sidebar-header.component.css'],
  providers : [ StorageService ]
})
export class MainSidebarHeaderComponent implements OnInit {

  public currentUser:any;

  constructor(
    private _storageService:StorageService
  ) { 
    this.currentUser = {};
  }

  ngOnInit() {
    this.currentUser = this._storageService.getCurrentUser();
  }  

}
