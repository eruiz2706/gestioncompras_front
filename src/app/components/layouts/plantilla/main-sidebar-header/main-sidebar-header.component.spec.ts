import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainSidebarHeaderComponent } from './main-sidebar-header.component';

describe('MainSidebarHeaderComponent', () => {
  let component: MainSidebarHeaderComponent;
  let fixture: ComponentFixture<MainSidebarHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainSidebarHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainSidebarHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
