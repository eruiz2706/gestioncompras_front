import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../../../services/storage.service';

@Component({
  selector: 'main-sidebar-container',
  templateUrl: './main-sidebar-container.component.html',
  styleUrls: ['./main-sidebar-container.component.css'],
  providers : [ StorageService ]
})
export class MainSidebarContainerComponent implements OnInit {

  public preload:boolean;
  public listaNavegacion:any;
  public currentUser:any;

  constructor(
    private _storageService:StorageService
  ) { 
    this.preload=false;
    this.currentUser = {};
    this.listaNavegacion = [];
  }

  ngOnInit() {
    this.currentUser = this._storageService.getCurrentUser();

    if(this.currentUser.idrol=='funcionario'){
      this.listaNavegacion.push({
        nombre : 'Gestionar requisiciones',
        url : '/requisicion-lista',
        icono : 'fa fa-list-alt'
      });
      this.listaNavegacion.push({
        nombre : 'Productos',
        url : '/producto-lista',
        icono : 'fa fa-cubes'
      });
    }

    if(this.currentUser.idrol=='director_area'){
      this.listaNavegacion.push({
        nombre : 'Revision de requisiciones',
        url : '/requisicion-revision',
        icono : 'fa fa-calendar-check-o'
      });
      this.listaNavegacion.push({
        nombre : 'Productos',
        url : '/producto-lista',
        icono : 'fa fa-cubes'
      });
    }

    if(this.currentUser.idrol=='director_compras'){
      this.listaNavegacion.push({
        nombre : 'Ordenes de compra',
        url : '/ordencompra-lista',
        icono : 'fa  fa-shopping-cart'
      });
      this.listaNavegacion.push({
        nombre : 'Entrega Requisiciones',
        url : '/requisicion-entrega',
        icono : 'fa fa-suitcase'
      });
    }

    if(this.currentUser.idrol=='almacen'){
      this.listaNavegacion.push({
        nombre : 'Productos',
        url : '/producto-lista',
        icono : 'fa fa-cubes'
      });
    }
    
    if(this.currentUser.idrol=='gerente'){
      this.listaNavegacion.push({
        nombre : 'Compras por Area',
        url : '/compras-area',
        icono : 'fa fa-bar-chart'
      });
      this.listaNavegacion.push({
        nombre : 'Compras producto top',
        url : '/compras-producto',
        icono : 'fa fa-bar-chart'
      });
      this.listaNavegacion.push({
        nombre : 'Compras por proveedor',
        url : '/compras-proveedor',
        icono : 'fa fa-bar-chart'
      });
      this.listaNavegacion.push({
        nombre : 'Tiempo de entrega',
        url : '/tiempo-entrega',
        icono : 'fa fa-bar-chart'
      });
    }
    this.preload=false;
  }
  
}
