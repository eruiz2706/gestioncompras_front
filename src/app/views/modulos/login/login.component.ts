import { Component, OnInit,Renderer2} from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { StorageService } from '../../../services/storage.service';
import { UsuarioService } from '../../../services/usuario.service';
declare var toastr :any;
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers : [ UsuarioService,StorageService ]
})
export class LoginComponent implements OnInit {

  public preload_envio:boolean;
  public usuario:string;
  public clave:string;
  public preload:boolean;
  
  constructor(
    private renderer: Renderer2,
    private _router : Router,
    private _userService : UsuarioService,
    private _storageService:StorageService
  ) { 
    this.renderer.addClass(document.body, 'color-fondo-login');
    this.preload=false;
    this.usuario='';
    this.clave='';
  }

  ngOnInit() {
    console.log(environment.MICROSERVICIO_COMPRAS);
    this._storageService.removeCurrentSession();
  }

  onSubmit(form){

    if(this.usuario=='' || this.clave==''){
      toastr.warning("Debe ingresar usuario y contraseña",'',{
        "timeOut": "3500"
      });
      return false;
    }

    this.preload = true;
      this._userService.login(this.usuario,this.clave).subscribe(
      response =>{
        //console.log(response);
        this._storageService.setCurrentSession(response.token,response.data);
        toastr.success(response.message,'',{
          "timeOut": "3500"
        });
        this.preload = false;
        this.renderer.removeClass(document.body,'color-fondo-login');
        this._router.navigate(['/principal'])
      },
      error =>{
        console.log(error); 
        this.preload = false;
        toastr.error(error.error.message,'',{
          "timeOut": "3500"
        });
      }
    );

  }

}
