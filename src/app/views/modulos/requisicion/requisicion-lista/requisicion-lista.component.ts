import { Component, OnInit } from '@angular/core';
import { RequisicionService } from '../../../../services/requisicion.service';
import { StorageService } from '../../../../services/storage.service';
declare var $: any;
declare var toastr :any;
declare var swal:any;

@Component({
  selector: 'app-requisicion-lista',
  templateUrl: './requisicion-lista.component.html',
  styleUrls: ['./requisicion-lista.component.css'],
  providers : [ RequisicionService, StorageService ]
})
export class RequisicionListaComponent implements OnInit {

  public preload:boolean;
  public preload_detalle:boolean;
  public userId:string;
  public listaDetalle:any;
  public listaRequisicion:any;
  
  constructor(
    private _requisicionService : RequisicionService,
    private _storage : StorageService
  ) { 
    this.userId='';
    this.preload=false;
    this.preload_detalle=false;
    this.listaRequisicion = [];
    this.listaDetalle = [];
  } 

  ngOnInit() {
    this.getListaFuncionario();
  }

  getListaFuncionario():void{
    this.userId =this._storage.getCurrentUserId();
    this.preload=true;
    this.listaRequisicion = [];
    this._requisicionService.getListaFuncionario(this.userId).subscribe(
      response =>{
        this.listaRequisicion = response.data;
        this.preload=false;
        console.log(response);
      },
      error =>{
        console.log(error);
        this.preload=false; 
        toastr.error(error.error.message,'',{
          "timeOut": "3500"
        });
      }
    );
  }

  abrirDetalle(idrequisicion):void{
    $('#modalDetallRequisicion').modal('show');
    this.preload_detalle=true;
    this.listaDetalle = [];
    this._requisicionService.getDetalle(idrequisicion).subscribe(
      response =>{
        this.preload_detalle=false;
        this.listaDetalle = response.data;
        console.log(response);
      }, 
      error =>{
        this.preload_detalle=false;
        toastr.error(error.error.message,'',{
          "timeOut": "3500"
        });
      }
    );
  }
  
  confirmarAnulacion(id):void{
    var vm=this;
    swal({
      title: "",
      text: "Seguro deseas anular la requisicion!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-primary",
      cancelButtonText: "Cancelar",
      confirmButtonText: "Confirmar!",
      closeOnConfirm: false
    },
    function(){
      vm.anularRequisicion(id);
    });
  }
 
  anularRequisicion(id):void{
    console.log(id);
    this._requisicionService.anular(id).subscribe(
      response =>{
        console.log(response);
        swal("Anulacion!",response.message, "success");
        this.getListaFuncionario();
      },
      error =>{
        toastr.error(error.error.message,'',{
          "timeOut": "3500"
        });
      }
    );
  }
}
