import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequisicionListaComponent } from './requisicion-lista.component';

describe('RequisicionListaComponent', () => {
  let component: RequisicionListaComponent;
  let fixture: ComponentFixture<RequisicionListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequisicionListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequisicionListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
