import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequisicionRevisionDetalleComponent } from './requisicion-revision-detalle.component';

describe('RequisicionRevisionDetalleComponent', () => {
  let component: RequisicionRevisionDetalleComponent;
  let fixture: ComponentFixture<RequisicionRevisionDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequisicionRevisionDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequisicionRevisionDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
