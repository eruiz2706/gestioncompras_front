import { Component, OnInit } from '@angular/core';
import { RequisicionService } from '../../../../services/requisicion.service';
import { Router, ActivatedRoute} from '@angular/router';
declare var $: any;
declare var toastr :any;
declare var swal:any;

@Component({
  selector: 'app-requisicion-revision-detalle',
  templateUrl: './requisicion-revision-detalle.component.html',
  styleUrls: ['./requisicion-revision-detalle.component.css']
})
export class RequisicionRevisionDetalleComponent implements OnInit {

  public preload:boolean;
  public detalleProdutos:any;
  public cantidad_total:number;
  public valor_total:number;
  public motivo_rechazo:string;
  public idRequisicion:string;

  constructor(
    private _router : Router,
    private _routeActive : ActivatedRoute,
    private _requisicionService : RequisicionService,
  ) { 
    this.detalleProdutos = [];
    this.cantidad_total = 0;
    this.valor_total = 0;
    this.preload = false;
  }

  ngOnInit() {
    this.getDetalle();
  }

  getDetalle():void{
    this._routeActive.params.subscribe(params =>{
      this.idRequisicion=params['id'];
      this.preload = true;
      this._requisicionService.getDetalle(this.idRequisicion).subscribe(
        response =>{
          this.detalleProdutos = response.data;
          this.recalcularTotales();
          this.preload = false;
        },
        error =>{
          this.preload = false;
          toastr.error(error.error.message,'',{
            "timeOut": "3500"
          });
        }
      );
    });

    
  }

  recalcularTotales():void{
    let items = this.detalleProdutos.length;
    this.cantidad_total = 0;
    for(let i=0;i<items;i++){
      this.cantidad_total++;
      this.valor_total += (this.detalleProdutos[i].cantidad_pedida* this.detalleProdutos[i].valor_unitario);
    }
  }

  aprobarRequisicon():void{
    var vm=this;
    swal({
      title: "",
      text: "Seguro deseas aprobar la requisicon!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-primary",
      cancelButtonText: "Cancelar",
      confirmButtonText: "Confirmar!",
      closeOnConfirm: true
    },function(){
      vm.aprobar();
    })
  }
  
  rechazarRequisicion():void{
    $('#modalRechazoRequisicion').modal('show');
  }
  aprobar():void{
    this._requisicionService.aprobar(this.idRequisicion).subscribe(
      response =>{
        toastr.success(response.message,'',{
          "timeOut": "3500"
        });
        this._router.navigate(['/requisicion-revision']);
      },
      error =>{
        toastr.error(error.error.message,'',{
          "timeOut": "3500"
        });
      }
    );
    
  }

  rechazar():void{
    let requisicion={motivo_rechazo:this.motivo_rechazo};
    this._requisicionService.rechazar(this.idRequisicion,requisicion).subscribe(
      response =>{
        toastr.success(response.message,'',{
          "timeOut": "3500"
        });
        $('#modalRechazoRequisicion').modal('hide');
        this._router.navigate(['/requisicion-revision']);
      },
      error =>{
        $('#modalRechazoRequisicion').modal('hide');
        toastr.error(error.error.message,'',{
          "timeOut": "3500"
        });
      }
    );
    
  }

}
