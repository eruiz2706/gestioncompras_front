import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { RequisicionService } from '../../../../services/requisicion.service';
import { ProductoService } from '../../../../services/producto.service';
import { StorageService } from '../../../../services/storage.service';
declare var toastr :any;
declare var swal:any;

@Component({
  selector: 'app-requisicion-crear',
  templateUrl: './requisicion-crear.component.html',
  styleUrls: ['./requisicion-crear.component.css'],
  providers : [ RequisicionService, ProductoService, StorageService ]
})
export class RequisicionCrearComponent implements OnInit {

  public preload_envio:boolean
  public listaProductos:any;
  public filtro_producto:String;
  public filtro_cantidad:number;
  public filtro_valor:number;
  public cantidad_total:number;
  public valor_total:number;
  public titulo:string;
  public motivo_compra:string;
  public select_producto:any;
  public userId:string;


  constructor(
    private _requisicionService : RequisicionService,
    private _router : Router,
    private _productoService : ProductoService,
    private _storage : StorageService
  ) { 
    this.preload_envio=false;
    this.listaProductos = [];
    this.filtro_producto = '';
    this.filtro_cantidad = null;
    this.filtro_valor = null;
    this.cantidad_total = 0;
    this.valor_total = 0;
    this.titulo = '';
    this.motivo_compra = '';
    this.select_producto = [];
    this.userId='';
  }

  ngOnInit() {
    this.userId =this._storage.getCurrentUserId();
    this.getSelectProducto();
  }

  agregarItem():void{
    this.listaProductos.push({
      codigo_producto : this.filtro_producto,
      nombre_producto : this.getSelectProductobyId(this.filtro_producto),
      cantidad_pedida : this.filtro_cantidad,
      valor_unitario : this.filtro_valor,
    });
    this.filtro_producto = '';
    this.filtro_cantidad = null;
    this.filtro_valor = null;
    this.recalcularTotales();
  }

  recalcularTotales():void{
    let items = this.listaProductos.length;
    this.cantidad_total = 0;
    this.valor_total = 0;
    for(let i=0;i<items;i++){
      this.cantidad_total++;
      this.valor_total += (this.listaProductos[i].cantidad_pedida * this.listaProductos[i].valor_unitario);
    }
  }

  quitarItem(index):void{
    this.listaProductos.splice(index,1);
    this.recalcularTotales();
  }

  guardar():void{
    var vm=this;
    this.preload_envio=true;
    let requisicion={
      userId : this.userId,
      titulo : this.titulo,
      motivo_compra : this.motivo_compra,
      valor_total : this.valor_total,
      detalle : this.listaProductos
    };
    console.log(requisicion);
    this._requisicionService.guardar(requisicion).subscribe(
      response =>{
        this.preload_envio=false;
        swal({
          title: "",
          text: response.message,
          type: "success"
        },
        function(){
          vm._router.navigate(['/requisicion-lista'])
        });
      },
      error =>{
        toastr.error(error.error.message,'',{
          "timeOut": "3500"
        });
        this.preload_envio=false;
      }
    );
  }

  getSelectProducto():void{
    this.select_producto = [];
    this._productoService.getLista().subscribe(
      response =>{
        this.select_producto = response.data;
        console.log(response);
      },
      error =>{
        toastr.error(error.error.message,'',{
          "timeOut": "3500"
        });
      }
    );
  }

  getSelectProductobyId(id):string{
    let length=this.select_producto.length;
    for(let i=0;i<length;i++){
      if(id==this.select_producto[i].id){
        return this.select_producto[i].nombre

      }
    }

    return '';
  }

}
