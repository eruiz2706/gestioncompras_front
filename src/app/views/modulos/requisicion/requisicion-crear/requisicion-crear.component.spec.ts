import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequisicionCrearComponent } from './requisicion-crear.component';

describe('RequisicionCrearComponent', () => {
  let component: RequisicionCrearComponent;
  let fixture: ComponentFixture<RequisicionCrearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequisicionCrearComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequisicionCrearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
