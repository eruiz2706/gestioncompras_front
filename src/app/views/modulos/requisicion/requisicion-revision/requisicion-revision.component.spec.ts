import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequisicionRevisionComponent } from './requisicion-revision.component';

describe('RequisicionRevisionComponent', () => {
  let component: RequisicionRevisionComponent;
  let fixture: ComponentFixture<RequisicionRevisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequisicionRevisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequisicionRevisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
