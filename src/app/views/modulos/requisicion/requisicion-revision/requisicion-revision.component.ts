import { Component, OnInit } from '@angular/core';
import { RequisicionService } from '../../../../services/requisicion.service';
import { StorageService } from '../../../../services/storage.service';
declare var toastr :any;

@Component({
  selector: 'app-requisicion-revision',
  templateUrl: './requisicion-revision.component.html',
  styleUrls: ['./requisicion-revision.component.css'],
  providers : [ RequisicionService, StorageService ]
})
export class RequisicionRevisionComponent implements OnInit {

  public preload:boolean;
  public listaRequisicion:any;
  public userId:string;
  
  constructor(
    private _requisicionService : RequisicionService,
    private _storage : StorageService
  ) { 
    this.listaRequisicion = [];
    this.preload = false;
    this.userId='';
  }

  ngOnInit() {
    this.userId =this._storage.getCurrentUserId();
    this.getListaRevision();
  }

  getListaRevision():void{
    this.preload = true;
    this._requisicionService.getListaRevision(this.userId).subscribe(
      response =>{
        this.listaRequisicion = response.data;
        this.preload = false;
        console.log(response);
      },
      error =>{
        console.log(error); 
        this.preload = false;
        toastr.error(error.error.message,'',{
          "timeOut": "3500"
        });
      }
    );
  }

}
