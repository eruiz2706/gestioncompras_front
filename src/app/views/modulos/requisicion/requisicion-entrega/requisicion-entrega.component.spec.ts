import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequisicionEntregaComponent } from './requisicion-entrega.component';

describe('RequisicionEntregaComponent', () => {
  let component: RequisicionEntregaComponent;
  let fixture: ComponentFixture<RequisicionEntregaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequisicionEntregaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequisicionEntregaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
