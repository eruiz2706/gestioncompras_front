import { Component, OnInit } from '@angular/core';
import { RequisicionService } from '../../../../services/requisicion.service';
import { StorageService } from '../../../../services/storage.service';
declare var $: any;
declare var toastr :any;

@Component({
  selector: 'app-requisicion-entrega',
  templateUrl: './requisicion-entrega.component.html',
  styleUrls: ['./requisicion-entrega.component.css'],
  providers : [ RequisicionService, StorageService ]
})
export class RequisicionEntregaComponent implements OnInit {

  public preload:boolean;
  public preload_detalle:boolean;
  public listaDetalle:any;
  public listaRequisicion:any;
  public cantidad_ingreso:number;
  public userId:string;

  constructor(
    private _requisicionService : RequisicionService,
    private _storage : StorageService
  ) { 
    this.preload=false;
    this.preload_detalle=false;
    this.listaRequisicion = [];
    this.listaDetalle = [];
    this.userId='';
    this.cantidad_ingreso = null;
  }

  ngOnInit() {
    this.userId =this._storage.getCurrentUserId();
    this.getListaEntrega();
  }

  getListaEntrega():void{
    this.preload=true;
    this._requisicionService.getListaEntrega(this.userId).subscribe(
      response =>{
        this.listaRequisicion = response.data;
        this.preload=false;
        console.log(response);
      },
      error =>{
        console.log(error);
        this.preload=false; 
        toastr.error(error.error.message,'',{
          "timeOut": "3500"
        });
      }
    );
  }

  abrirEntrega(idrequisicion):void{
    $('#modalEntrega').modal('show');
    
  }
}
