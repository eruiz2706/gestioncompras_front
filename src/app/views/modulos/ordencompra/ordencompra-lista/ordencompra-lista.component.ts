import { Component, OnInit } from '@angular/core';
import { OrdencompraService } from '../../../../services/ordencompra.service';
import { StorageService } from '../../../../services/storage.service';
declare var $: any;
declare var toastr :any;

@Component({
  selector: 'app-ordencompra-lista',
  templateUrl: './ordencompra-lista.component.html',
  styleUrls: ['./ordencompra-lista.component.css'],
  providers : [ OrdencompraService, StorageService ]
})
export class OrdencompraListaComponent implements OnInit {

  public preload:boolean;
  public preload_detalle:boolean;
  public listaOrdenCompra:any;
  public listaDetalle:any;
  public userId:string;

  constructor(
    private _ordenCompraService : OrdencompraService,
    private _storage : StorageService
  ) { 
    this.listaOrdenCompra = [];
    this.listaDetalle = [];
    this.preload = false;
    this.preload_detalle = false;
    this.userId = '';
  } 
  
  ngOnInit() {
    this.getLista();
  }

  getLista():void{
    this.userId =this._storage.getCurrentUserId();
    this.preload = true;
    this.listaOrdenCompra = [];
    this._ordenCompraService.getLista(this.userId).subscribe(
      response =>{
        this.listaOrdenCompra = response.data;
        this.preload = false;
        console.log(response);
      },
      error =>{
        console.log(error); 
        this.preload = false;
        toastr.error(error.error.message,'',{
          "timeOut": "3500"
        });
      }
    );
  }

  abrirDetalleOrden(idorden):void{
    $('#modalDetalleOrden').modal('show');
    this.preload_detalle = true;
    this.listaDetalle = [];
    this._ordenCompraService.getDetalle(idorden).subscribe(
      response =>{
        this.listaDetalle = response.data;
        this.preload_detalle = false;
        console.log(response);
      },
      error =>{
        this.preload_detalle = false;
        toastr.error(error.error.message,'',{
          "timeOut": "3500"
        });
      }
    );
  }

  

}
