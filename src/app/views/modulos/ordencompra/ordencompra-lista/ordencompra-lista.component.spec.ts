import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdencompraListaComponent } from './ordencompra-lista.component';

describe('OrdencompraListaComponent', () => {
  let component: OrdencompraListaComponent;
  let fixture: ComponentFixture<OrdencompraListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdencompraListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdencompraListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
