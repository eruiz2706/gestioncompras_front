import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { ProductoService } from '../../../../services/producto.service';
import {Producto} from '../../../../models/producto';
declare var toastr :any;


@Component({
  selector: 'app-producto-crear',
  templateUrl: './producto-crear.component.html',
  styleUrls: ['./producto-crear.component.css'],
  providers : [ ProductoService ]
})
export class ProductoCrearComponent implements OnInit {

  public preload_guardar:boolean;
  public nombre:string;
  public idcategoria:number;

  constructor(
    private _productoService : ProductoService,
    private _router : Router,
  ) { 
    this.preload_guardar = false;
    this.nombre = '';
    this.idcategoria = 0;
  }

  ngOnInit() {
  }

  guardar():void{
    let producto = new Producto(0,this.nombre,this.idcategoria);
    this.preload_guardar = true;
    this._productoService.guardar(producto).subscribe(
      response =>{
        toastr.success(response.message,'',{
          "timeOut": "3500"
        });
        this.preload_guardar = false;
        this._router.navigate(['/producto-lista'])
      },
      error =>{
        console.log(error); 
        this.preload_guardar = false;
        toastr.error(error.error.message,'',{
          "timeOut": "3500"
        });
      }
    );
  }

}
