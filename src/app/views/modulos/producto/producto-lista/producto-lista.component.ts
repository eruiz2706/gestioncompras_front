import { Component, OnInit } from '@angular/core';
import { ProductoService } from '../../../../services/producto.service';
declare var $: any;
declare var toastr :any;

@Component({ 
  selector: 'app-producto-lista',
  templateUrl: './producto-lista.component.html',
  styleUrls: ['./producto-lista.component.css'],
  providers : [ ProductoService ]
})
export class ProductoListaComponent implements OnInit {

  public preload:boolean;
  public preload_stock:boolean;
  public listaProductos:any;
  public listaStock:any;

  constructor(
    private _productoService : ProductoService,
  ) { 
    this.listaProductos = [];
    this.listaStock = [];
    this.preload = false;
  } 

  ngOnInit() {
    this.getLista();
  }

  getLista():void{
    this.preload = true;
    this.listaProductos = [];
    this._productoService.getLista().subscribe(
      response =>{
        this.listaProductos = response.data;
        this.preload = false;
        console.log(response);
      },
      error =>{ 
        console.log(error); 
        this.preload = false;
        toastr.error(error.error.message,'',{
          "timeOut": "3500"
        });
      }
    );
  }

  abrirStock(id):void{
    $('#modalStock').modal('show');
    this.preload_stock = true;
    this.listaStock = [];
    this._productoService.stockById(id).subscribe(
      response =>{
        this.listaStock = response.data;
        this.preload_stock = false;
        console.log(response.data);
      },
      error =>{
        console.log(error); 
        this.preload_stock = false;
        toastr.error(error.error.message,'',{
          "timeOut": "3500"
        });
      }
    );
  }

  abrirEntradaStock():void{
    $('#modalStockEntrada').modal('show');
  }

  entradaStock():void{
    let id=12
    $('#modalStockEntrada').modal('hide');
    this._productoService.entradainv(id,id,id).subscribe(
      response =>{
        toastr.success(response.message,'',{
          "timeOut": "3500"
        });
        console.log(response.data);
      },
      error =>{
        console.log(error); 
        toastr.error(error.error.message,'',{
          "timeOut": "3500"
        });
      }
    );
  }
  
}
