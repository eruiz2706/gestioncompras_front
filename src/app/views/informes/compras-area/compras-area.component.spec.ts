import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComprasAreaComponent } from './compras-area.component';

describe('ComprasAreaComponent', () => {
  let component: ComprasAreaComponent;
  let fixture: ComponentFixture<ComprasAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComprasAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComprasAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
