import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiempoEntregaComponent } from './tiempo-entrega.component';

describe('TiempoEntregaComponent', () => {
  let component: TiempoEntregaComponent;
  let fixture: ComponentFixture<TiempoEntregaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiempoEntregaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiempoEntregaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
