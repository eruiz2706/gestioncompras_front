import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tiempo-entrega',
  templateUrl: './tiempo-entrega.component.html',
  styleUrls: ['./tiempo-entrega.component.css']
})
export class TiempoEntregaComponent implements OnInit {

  public fechaInicial:any;
  public fechaFinal:any;

  constructor() { }

  ngOnInit() {
    this.fechaInicial = '';
    this.fechaFinal = '';
  }

}
