import { Component, OnInit } from '@angular/core';
import { OrdencompraService } from '../../../services/ordencompra.service';
declare var toastr :any;

@Component({
  selector: 'app-compras-producto',
  templateUrl: './compras-producto.component.html',
  styleUrls: ['./compras-producto.component.css'],
  providers : [ OrdencompraService ]
})
export class ComprasProductoComponent implements OnInit {

  public fechaInicial:string;
  public fechaFinal:string;
  public preload:boolean;
  public listaInforme:any;

  constructor(
    private _ordenCompraService : OrdencompraService
  ) { 
    this.preload = false;
    this.listaInforme = [];
    this.fechaInicial = '';
    this.fechaFinal = '';
  }

  ngOnInit() {
  }

  generarInforme():void{
    this.preload = true;
    this.listaInforme = [];
    this._ordenCompraService.getTopProducto(this.fechaInicial,this.fechaFinal).subscribe(
      response =>{
        this.listaInforme = response.data;
        this.preload = false;
        console.log(response);
      },
      error =>{
        console.log(error); 
        this.preload = false;
        toastr.error(error.error.message,'',{
          "timeOut": "3500"
        });
      }
    );
  }
}
