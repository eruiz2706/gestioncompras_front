import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';

@Injectable()
export class AuthorizatedGuard implements CanActivate {
  constructor(
    private router: Router
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot) {
    let currentUser=JSON.parse(localStorage.getItem('currentUser'));
    
  if(localStorage.getItem('identity') == null){
      this.router.navigate(['/login']);
      return false;
    }

    /*if (route.data.roles && route.data.roles.indexOf(currentUser.idrol) === -1) {
      this.router.navigate(['/login']);
      return false;
    }*/

    return true;
  }
}
