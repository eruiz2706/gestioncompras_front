import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { GLOBAL } from '../global';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  public API_URL:string;

  constructor(
    public _http : HttpClient
  ) {
      this.API_URL =environment.MICROSERVICIO_USUARIO;
  }
 
  public login(usuario,clave) : Observable<any>{
    let headers =new HttpHeaders().set('Content-Type','application/json');
    return this._http.get(`${this.API_URL}/usuarios/login/?usuario=${usuario}&clave=${clave}`,{headers:headers});
  }

}
