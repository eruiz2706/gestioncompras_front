import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { GLOBAL } from '../global';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  private API_URL:string;

  constructor(
    private _http : HttpClient
  ) {
      this.API_URL =environment.MICROSERVICIO_PRODUCTO;
  }
 
  public getLista() : Observable<any>{ 
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.get(`${this.API_URL}/productos`,{headers:headers});
  }

  public stockById(id) : Observable<any>{
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.get(`${this.API_URL}/productos/${id}/stock`,{headers:headers});
  }

  public guardar(producto) : Observable<any>{
    let body =producto;
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.post(`${this.API_URL}/productos`,body,{headers:headers});
  }

  public actualizar(id,producto) : Observable<any>{
    let body =producto;
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.put(`${this.API_URL}/productos/${id}`,body,{headers:headers});
  }

  public getById(id) : Observable<any>{
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.get(`${this.API_URL}/productos/${id}`,{headers:headers});
  }

  public entradainv(id,odencompra,cantidad) : Observable<any>{
    let body ={
      ordenCompra : odencompra,
      cantidad : cantidad
    };
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.put(`${this.API_URL}/productos/${id}/entradainv`,body,{headers:headers});
  }
}
