import { TestBed } from '@angular/core/testing';

import { RequisicionService } from './requisicion.service';

describe('RequisicionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RequisicionService = TestBed.get(RequisicionService);
    expect(service).toBeTruthy();
  });
});
