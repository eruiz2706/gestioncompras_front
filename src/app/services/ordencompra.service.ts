import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { GLOBAL } from '../global';

@Injectable({
  providedIn: 'root'
})
export class OrdencompraService {

  private API_URL:string;

  constructor(
    private _http : HttpClient
  ) {
      this.API_URL =environment.MICROSERVICIO_COMPRAS;
  }
 
  public getLista(idUsuario) : Observable<any>{ 
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.get(`${this.API_URL}/compras/ciudad/${idUsuario}`,{headers:headers});
  }

  public getDetalle(idorden) : Observable<any>{ 
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.get(`${this.API_URL}/compras/${idorden}/detalle`,{headers:headers});
  }
  
  public getInfoArea(fechaInicial,fechaFinal) : Observable<any>{ 
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.get(`${this.API_URL}/compras/infoarea/?fechaInicial=${fechaInicial}&fechaFinal=${fechaFinal}`,{headers:headers});
  }

  public getTopProducto(fechaInicial,fechaFinal) : Observable<any>{ 
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.get(`${this.API_URL}/compras/topproducto/?fechaInicial=${fechaInicial}&fechaFinal=${fechaFinal}`,{headers:headers});
  }

  public getTopProveedor(fechaInicial,fechaFinal) : Observable<any>{ 
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.get(`${this.API_URL}/compras/topproveedor/?fechaInicial=${fechaInicial}&fechaFinal=${fechaFinal}`,{headers:headers});
  }

}
