import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() {
  }

  setCurrentSession(identity:string,currentUser :any): void {
    localStorage.setItem('identity',identity);
    localStorage.setItem('UserId',currentUser.id);
    localStorage.setItem('currentUser',JSON.stringify({
      "id": currentUser.id,
      "nombre": currentUser.nombre_usuario,
      "idrol" : currentUser.codigo_rol,
      "rol" : currentUser.nombre_rol,
      "imagen" : currentUser.imagen
    }));
    localStorage.setItem('UserId',currentUser.id);
  }

  removeCurrentSession(): void {
    localStorage.removeItem('identity');
    localStorage.removeItem('currentUser');
  }

  getIdentity(): string {
    let identity  =localStorage.getItem('identity');
    if(identity==null)identity='';
    return identity;
  }

  getCurrentUser(){
    let currentUser=localStorage.getItem('currentUser');
    if(currentUser==null)currentUser='';
    console.log(currentUser);
    return JSON.parse(currentUser);
  }

  getCurrentUserId(){
    let userId  =localStorage.getItem('UserId');
    return userId;
  }

  isAuthenticated(): boolean {
    return (this.getIdentity() == '') ? false : true;
  }
}
