import { TestBed } from '@angular/core/testing';

import { OrdencompraService } from './ordencompra.service';

describe('OrdencompraService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrdencompraService = TestBed.get(OrdencompraService);
    expect(service).toBeTruthy();
  });
});
