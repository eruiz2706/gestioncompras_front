import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { GLOBAL } from '../global';

@Injectable({
  providedIn: 'root'
})
export class RequisicionService {

  private API_URL:string;

  constructor(
    private _http : HttpClient
  ) {
      this.API_URL =environment.MICROSERVICIO_REQUISICION;
  }
 
  public getListaFuncionario(idfuncionario) : Observable<any>{
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.get(`${this.API_URL}/requisicion/funcionario/${idfuncionario}`,{headers:headers});
  }

  public getDetalle(idrequisicion) : Observable<any>{
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.get(`${this.API_URL}/requisicion/${idrequisicion}/detalle`,{headers:headers});
  }

  public anular(idrequisicion) : Observable<any>{
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.put(`${this.API_URL}/requisicion/${idrequisicion}/anular`,{headers:headers});
  }

  public guardar(requisicion) : Observable<any>{
    let body = requisicion;
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.post(`${this.API_URL}/requisicion/`,body,{headers:headers});
  }

  public getListaRevision(idfefe) : Observable<any>{
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.get(`${this.API_URL}/requisicion/jefe/${idfefe}`,{headers:headers});
  }

  public aprobar(idrequisicion) : Observable<any>{
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.put(`${this.API_URL}/requisicion/${idrequisicion}/aprobar`,{headers:headers});
  }

  public rechazar(idrequisicion,requisicion) : Observable<any>{
    let body = requisicion;
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.put(`${this.API_URL}/requisicion/${idrequisicion}/rechazar`,body,{headers:headers});
  }

  public getListaEntrega(idusuario) : Observable<any>{
    let headers =new HttpHeaders().set('Content-Type','application/json'); 
    return this._http.get(`${this.API_URL}/requisicion/entrega/${idusuario}`,{headers:headers});
  }
}
