import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {AuthorizatedGuard} from "./guards/authorizated.guard";
import { LoginComponent } from './views/modulos/login/login.component';
import { PrincipalComponent } from './views/modulos/principal/principal.component';
import { RequisicionCrearComponent } from './views/modulos/requisicion/requisicion-crear/requisicion-crear.component';
import { RequisicionListaComponent } from './views/modulos/requisicion/requisicion-lista/requisicion-lista.component';
import { RequisicionRevisionComponent } from './views/modulos/requisicion/requisicion-revision/requisicion-revision.component';
import { RequisicionEntregaComponent } from './views/modulos/requisicion/requisicion-entrega/requisicion-entrega.component';
import { RequisicionRevisionDetalleComponent } from './views/modulos/requisicion/requisicion-revision-detalle/requisicion-revision-detalle.component';
import { OrdencompraListaComponent } from './views/modulos/ordencompra/ordencompra-lista/ordencompra-lista.component';
import { ProductoListaComponent } from './views/modulos/producto/producto-lista/producto-lista.component';
import { ProductoCrearComponent } from './views/modulos/producto/producto-crear/producto-crear.component';
import { ComprasAreaComponent } from './views/informes/compras-area/compras-area.component';
import { ComprasProductoComponent } from './views/informes/compras-producto/compras-producto.component';
import { ComprasProveedorComponent } from './views/informes/compras-proveedor/compras-proveedor.component';
import { TiempoEntregaComponent } from './views/informes/tiempo-entrega/tiempo-entrega.component';

const appRoutes : Routes =[
  {path :'login' ,component :LoginComponent},
  {path :'principal' ,component :PrincipalComponent, canActivate: [ AuthorizatedGuard ]},
  {path :'requisicion-crear' ,component :RequisicionCrearComponent, canActivate: [ AuthorizatedGuard ]},
  {path :'requisicion-entrega' ,component :RequisicionEntregaComponent, canActivate: [ AuthorizatedGuard ]},
  {path :'requisicion-revision' ,component :RequisicionRevisionComponent, canActivate: [ AuthorizatedGuard ]},
  {path :'requisicion-revision-detalle/:id' ,component :RequisicionRevisionDetalleComponent, canActivate: [ AuthorizatedGuard ]},
  {path :'requisicion-lista' ,component :RequisicionListaComponent, canActivate: [ AuthorizatedGuard ],data: { roles: ['director_area'] }},
  {path :'ordencompra-lista' ,component :OrdencompraListaComponent, canActivate: [ AuthorizatedGuard ]},
  {path :'producto-lista' ,component :ProductoListaComponent, canActivate: [ AuthorizatedGuard ]},
  {path :'producto-crear' ,component :ProductoCrearComponent, canActivate: [ AuthorizatedGuard ]},
  {path :'compras-area' ,component :ComprasAreaComponent, canActivate: [ AuthorizatedGuard ]},
  {path :'compras-producto' ,component :ComprasProductoComponent, canActivate: [ AuthorizatedGuard ]},
  {path :'compras-proveedor' ,component :ComprasProveedorComponent, canActivate: [ AuthorizatedGuard ]},
  {path :'tiempo-entrega' ,component :TiempoEntregaComponent, canActivate: [ AuthorizatedGuard ]},
  {path :'',component :LoginComponent},
  {path :'**' ,component :LoginComponent},
];

export const appRoutingProviders: any[] = [];
export const routing : ModuleWithProviders = RouterModule.forRoot(appRoutes);
