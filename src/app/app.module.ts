import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {AuthorizatedGuard} from "./guards/authorizated.guard";

//import { AppRoutingModule } from './app-routing.module';
import { routing, appRoutingProviders } from './app.routing';
import { AppComponent } from './app.component';
import { MainFooterComponent } from './components/layouts/plantilla/main-footer/main-footer.component';
import { MainSidebarHeaderComponent } from './components/layouts/plantilla/main-sidebar-header/main-sidebar-header.component';
import { MainSidebarContainerComponent } from './components/layouts/plantilla/main-sidebar-container/main-sidebar-container.component';
import { MainControlSidebarComponent } from './components/layouts/plantilla/main-control-sidebar/main-control-sidebar.component';
import { LoginComponent } from './views/modulos/login/login.component';
import { PrincipalComponent } from './views/modulos/principal/principal.component';
import { RequisicionCrearComponent } from './views/modulos/requisicion/requisicion-crear/requisicion-crear.component';
import { RequisicionListaComponent } from './views/modulos/requisicion/requisicion-lista/requisicion-lista.component';
import { RequisicionRevisionComponent } from './views/modulos/requisicion/requisicion-revision/requisicion-revision.component';
import { RequisicionEntregaComponent } from './views/modulos/requisicion/requisicion-entrega/requisicion-entrega.component';
import { RequisicionRevisionDetalleComponent } from './views/modulos/requisicion/requisicion-revision-detalle/requisicion-revision-detalle.component';
import { OrdencompraListaComponent } from './views/modulos/ordencompra/ordencompra-lista/ordencompra-lista.component';
import { ProductoListaComponent } from './views/modulos/producto/producto-lista/producto-lista.component';
import { ProductoCrearComponent } from './views/modulos/producto/producto-crear/producto-crear.component';
import { ComprasAreaComponent } from './views/informes/compras-area/compras-area.component';
import { ComprasProveedorComponent } from './views/informes/compras-proveedor/compras-proveedor.component';
import { TiempoEntregaComponent } from './views/informes/tiempo-entrega/tiempo-entrega.component';
import { ComprasProductoComponent } from './views/informes/compras-producto/compras-producto.component';

@NgModule({
  declarations: [
    AppComponent,
    MainFooterComponent,
    MainSidebarHeaderComponent,
    MainSidebarContainerComponent,
    MainControlSidebarComponent,
    LoginComponent,
    PrincipalComponent,
    RequisicionCrearComponent,
    RequisicionListaComponent,
    RequisicionRevisionComponent,
    RequisicionEntregaComponent,
    RequisicionRevisionDetalleComponent,
    OrdencompraListaComponent,
    ProductoListaComponent,
    ProductoCrearComponent,
    ComprasAreaComponent,
    ComprasProveedorComponent,
    TiempoEntregaComponent,
    ComprasProductoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    routing
  ],
  providers: [
    appRoutingProviders,
    AuthorizatedGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
